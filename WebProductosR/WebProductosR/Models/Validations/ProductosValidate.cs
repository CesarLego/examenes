﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebProductosR.Models
{
    [MetadataType(typeof(Productos.MetaData))]
    public partial class Productos
    {
        sealed class MetaData
        {
            [Key]
            public int Product_id;

            [Required(ErrorMessage = "Ingresa el código del producto")]
            public string Product_code;

            [Required(ErrorMessage = "Ingresa el nombre del producto")]
            public string Product_name;


            [Required(ErrorMessage = "Describe la presentación del producto")]
            public string Product_desc;

            [Required(ErrorMessage = "Ingresa el precio del producto")]
            public Nullable<int> Product_price;
        }
    }
}