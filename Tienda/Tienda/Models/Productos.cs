﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace Tienda.Models
{
    public class Productos
    {
        [Key]
        [Display(Name = "N°")]
        public int Product_id { get; set; }

        [Required(ErrorMessage = "Ingresa el código del producto")]
        [Display(Name = "Código del Producto")]
        public string Product_code { get; set; }

        [Required(ErrorMessage = "Ingresa el nombre del producto")]
        [Display(Name = "Nombre del Producto")]
        public string Product_name { get; set; }

        [Required(ErrorMessage = "Ingresa la presentación del producto")]
        [Display(Name = "Descripción")]
        public string Product_desc { get; set; }

        [Required(ErrorMessage = "Ingresa el precio del producto")]
        [Display(Name = "Precio del producto")]
        public int Product_price { get; set; }
    }
}
