﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Tienda.Models;

namespace Tienda.Controllers
{
    public class ProductosController : Controller
    {
        private readonly TiendaContext _context;

        public ProductosController(TiendaContext context)
        {
            _context = context;


        }


        [HttpGet]
        public async Task<IActionResult> Index(string searchp)
        {
            ViewData["GetEmployeeDetails"] = searchp;
            var buscaquery = from x in _context.Productos select x;

            if (!String.IsNullOrEmpty(searchp))
            {
                buscaquery = buscaquery.Where(x => x.Product_name.Contains(searchp) || x.Product_code.Contains(searchp));
            }

            return View(await buscaquery.AsNoTracking().ToListAsync());
        }



        public IActionResult Index()
        {
            var displaydata = _context.Productos.ToList();
            return View(displaydata);

        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult  Create(Productos nProduct)
        {
            if (ModelState.IsValid)
            {
                _context.Add(nProduct);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(nProduct);
        }

        public async Task<IActionResult> Editar(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var getEmpDetail = await _context.Productos.FindAsync(id);

            return View(getEmpDetail);


        }

        [HttpPost]
        public async Task<IActionResult> Editar(Productos actualCustomer)
        {
            if (ModelState.IsValid)
            {
                _context.Update(actualCustomer);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(actualCustomer);

        }

        public async Task<IActionResult> Eliminar(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var getEmpDetail = await _context.Productos.FindAsync(id);

            return View(getEmpDetail);


        }

        [HttpPost]
        public async Task<IActionResult> Eliminar(int id)
        {
            var getEmpdetail = await _context.Productos.FindAsync(id);
            _context.Productos.Remove(getEmpdetail);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index");
        }


        public async Task<IActionResult> Detalles(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var getDetail = await _context.Productos.FindAsync(id);

            return View(getDetail);
        }

    }
}
